# ADFeedback-Pod-library

[![CI Status](https://img.shields.io/travis/Sagar/ADFeedback-Pod-library.svg?style=flat)](https://travis-ci.org/Sagar/ADFeedback-Pod-library)
[![Version](https://img.shields.io/cocoapods/v/ADFeedback-Pod-library.svg?style=flat)](https://cocoapods.org/pods/ADFeedback-Pod-library)
[![License](https://img.shields.io/cocoapods/l/ADFeedback-Pod-library.svg?style=flat)](https://cocoapods.org/pods/ADFeedback-Pod-library)
[![Platform](https://img.shields.io/cocoapods/p/ADFeedback-Pod-library.svg?style=flat)](https://cocoapods.org/pods/ADFeedback-Pod-library)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ADFeedback-Pod-library is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ADFeedback-Pod-library'
```

## Author

Sagar, sagar.desai@xische.com

## License

ADFeedback-Pod-library is available under the MIT license. See the LICENSE file for more info.
