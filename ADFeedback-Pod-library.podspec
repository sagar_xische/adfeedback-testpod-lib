#
# Be sure to run `pod lib lint ADFeedback-Pod-library.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ADFeedback-Pod-library'
  s.version          = '0.1.0'
  s.summary          = 'ADFeedback-Pod-library is used for bug tracking and is a open soure library'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "ADFeedback is a clear, inclusive, modular, flexible, and consistent guide for bug tracking and reporting practices."

  s.homepage         = 'https://github.com/Sagar/ADFeedback-Pod-library'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'AdFeedback' => 'adFeedback@support.com' }
  s.source           = { :git => 'https://gitlab.abudhabi.ae/TAMM/mobile-native-apps/ios-bug-report-lib.git', :branch => 'feature/BaseSetup', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'

  s.source_files = 'ADFeedback-Pod-library/Classes/**/*'
  
  # s.resource_bundles = {
  #   'ADFeedback-Pod-library' => ['ADFeedback-Pod-library/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
   s.dependency 'Alamofire', '~> 5'
end
